from django.views.generic import TemplateView

class Beranda(TemplateView):
    template_name = 'beranda.html'
    
class Riwayat(TemplateView):
    template_name = 'riwayat.html'

class Skill(TemplateView):
    template_name = 'skill.html'

class Tentangsaya(TemplateView):
    template_name = 'tentangsaya.html'