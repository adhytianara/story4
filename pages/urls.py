from django.urls import path

from .views import Beranda, Riwayat, Skill, Tentangsaya

urlpatterns = [
    path('', Beranda.as_view(), name='beranda'),
    path('beranda/', Beranda.as_view(), name='beranda'),
    path('riwayat/', Riwayat.as_view(), name='riwayat'),
    path('skill/', Skill.as_view(), name='skill'),
    path('tentang-saya/', Tentangsaya.as_view(), name='tentang-saya'),
]